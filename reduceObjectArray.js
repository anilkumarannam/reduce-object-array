const reduceObjectArray = inputArray => {
    if (inputArray) {
        return inputArray.reduce((previousObject, currentOject) => {
            const { name, ...rest } = currentOject;
            return { ...previousObject, [name]: { ...rest } };
        }, {});
    }
}

const result = [{
    name: "bill-marley",
    age: 23,
    dob: "23/2/2022"
}, {
    name: "john-spectre",
    age: 56,
    dob: "2/2/1969"
}, {
    name: "larry-pascal",
    age: 19,
    dob: "3/6/2002"
}];

const result1 = reduceObjectArray(result);
console.log(result1);